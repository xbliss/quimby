= Quimby

Quimby is a tool that forwards events from an `evdev` input device to a Linux USB HID gadget.
