cmake_minimum_required(VERSION 3.10.0)
project(quimby
    VERSION 0.0.1
    HOMEPAGE_URL "https://nerdcruft.net"
    LANGUAGES CXX
)

list (APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
include (nc)

include_directories("${CMAKE_CURRENT_SOURCE_DIR}")

add_subdirectory(cruft/util)
add_subdirectory(cruft/evdev)

add_executable(relay relay.cpp)

install(
    FILES
        systemd/system/quimby.service
    DESTINATION
        /usr/lib/systemd/system/
)

install(
    FILES
        udev/quimby.rules
    DESTINATION
        /etc/udev/rules.d/
)

install(
    PROGRAMS
        quimby-relay
        quimby-cleanup
        quimby-setup
    DESTINATION
        bin
)

include (CPack)